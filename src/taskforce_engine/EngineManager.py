import logging
import sys
import os
from collections import defaultdict

from taskforce_engine import EngineInterface
from taskforce_common.utils import FileMap, getSourceCodeFileName
from taskforce_common import ExceptionMessage, GroupDefinitionLoader
from taskforce_library import calculateHash
from taskforce_common import BLOCK_FILE_EXTENSION


class EngineManager(EngineInterface):
    """
    The EngineManager is an extension of the EngineInterface.  The purpose to add in management of TaskForce client-
    side content libraries in order to provide an end-to-end interface with the Engine.

    """
    # This regex will match all files that end with .py and .group
    FileMapRegex = '(.+\.py$)|(.+\\' + BLOCK_FILE_EXTENSION + '$)'

    def __init__(self, clientPort):
        EngineInterface.__init__(self, clientPort)

        self.fileMap = FileMap([EngineManager.FileMapRegex])
        self.logger = logging.getLogger(__name__)
        self.parents = defaultdict(list)

    def addLocalLibraryFolder(self, path):
        """Add a library folder to the local library file map.

        This will add the parent path to the Python path

        Args:
            path: path to the folder as a string
        """
        self.fileMap.addFolder(path)
        parent_path = os.path.abspath(os.path.join(path, os.pardir))
        self.parents[parent_path].append(path)
        if parent_path not in sys.path:
            sys.path = [parent_path] + sys.path

    def clearLocalLibraryFolders(self):
        """Remove all folders from the local library file map"""
        for parent, paths in self.parents:
            for path in paths:
                self.removeLocalLibraryFolder(path)

    def libraryAddModuleFromGroup(self, groupName):
        """Recursively add modules to the library contained within a Group

        Args:
            groupName: Library-relative path for the .group file.

        Returns:
            True if successful, otherwise an ExceptionMessage
        """
        definition = GroupDefinitionLoader.loadDefinition(groupName, self.fileMap.getMap())

        for tasksInGroup in definition['group'].get('tasks', []):
            task = tasksInGroup['task']

            if not self.libraryModuleMatchesRemote(task['module']):
                response = self.libraryAddModule(module_name=task['module'])

                if isinstance(response, ExceptionMessage):
                    self.logger.error(response)
                    return response
                else:
                    self.logger.info("Adding library module {}".format(task['module']))

        return True

    def libraryModuleMatchesRemote(self, module_name):
        """Check if a local library module matches with a module in the Engine's library.

        Args:
            module_name: name of the module as a string

        Returns:
            True if the module is the Engine exists and has the same hash, otherwise False.
        """
        filename = getSourceCodeFileName(module_name)
        hashLocal = calculateHash(filename)
        info = self.libraryGetInfo()

        try:
            hashRemote = info[module_name]["hash"]
        except KeyError:
            # the module is missing
            return False

        return hashLocal == hashRemote

    def groupDeployByName(self, groupName):
        """ Deploy a group using the library-relative pathname

        Args:
            groupName: Library=relative group name as a string

        Returns:
            True on success
        """
        self.libraryAddModuleFromGroup(groupName)
        definition = GroupDefinitionLoader.loadDefinition(groupName, self.fileMap.getMap())
        return self.groupDeploy(definition)

    def removeLocalLibraryFolder(self, path):
        """Remove a library folder from the local library file map

        This will remove the parent path from the Python path if it is the last child of the parent in the map.

        Args:
            path: path to the folder as a string
        """
        self.fileMap.removeFolder(path)
        parent_path = os.path.abspath(os.path.join(path, os.pardir))
        self.parents[parent_path].remove(path)
        if len(self.parents[parent_path]) == 0:
            self.parents.pop(parent_path)
            sys.path.remove(parent_path)

    def taskDeploySimple(self, name, module_name, class_name):
        """ Deploy a stand-alone task.

        Args:
            name: instance name of the task as a string
            module_name: name of the module as a string
            class_name: name of the class of the task as a string

        Returns:
            True on success
        """
        return self.taskDeploy({'task': {'name': name, 'module': module_name, 'class': class_name, 'parameters': {}}})



    # #  LOAD ALL MODULES FROM A PACKAGE INTO THE REMOTE LIBRARY.  IT ONLY LOADS THE ONES THAT ARE IN THE PACKAGE
    # #  AND ARE IN THE PACKAGE PATH
    # def libraryLoadPackage(self, package_name):
    #     """
    #     Load all the modules from a package to the remote library.  It only loads the ones that are in the package and the package path
    #
    #     @type package_name string
    #     @param package_name Name of the package
    #
    #     @type return Response
    #     @return Response exception if it failed.  None if succeeded
    #     """
    #     try:
    #         package = importlib.import_module(package_name)
    #     except Exception as ex:
    #         self.logger.info("libraryLoadPackage:  Could not load {}".format(package_name))
    #         return ex
    #
    #     localMap = FileMap(('.py'))
    #     for path in package.__path__:
    #         localMap.addFolder(path)
    #
    #     map = localMap.getMap()
    #
    #     for key, value in map.iteritems():
    #         if "__init__.py" in key:
    #             continue
    #
    #         list = key.split('/')
    #         tempKey = ''
    #         valid = True
    #         for dir in list:
    #             if '.py' in dir:
    #                 continue
    #
    #             tempKey = tempKey + dir + '/'
    #             try:
    #                 if not map[tempKey + '__init__.py']:
    #                     valid = False
    #                     break
    #             except:
    #                 valid = False
    #                 break
    #
    #         if valid:
    #             module_name = key.replace('.py','')
    #             module_name = module_name.replace('/','.')
    #             if self.libraryIsModuleLoadNeeded(module_name):
    #                 response = self.libraryAddModule(module_name)
    #
    #                 if isinstance(response, ExceptionMessage):
    #                     self.logger.error(response)
    #                     return response
    #                 else:
    #                     self.logger.info("Adding library module {}".format(module_name))
    #
    #     return None
