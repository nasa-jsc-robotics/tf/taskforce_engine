import unittest
import os
import shutil
import sys
import time
import logging

from taskforce_engine import Engine
from taskforce_engine import EngineInterface
from taskforce_common import QueuePubSub, QueueReqRep
from taskforce_common import ExceptionMessage
from taskforce_common import TaskState

from taskforce_common import GroupDefinitionLoader
from taskforce_common.utils import FileMap, getSourceCode

TEST_FOLDER = os.path.dirname(os.path.realpath(__file__))
TASK_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'task_library')
BLOCK_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'block_library')
ENGINE_LOCAL_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'engine_local_library_path')


TestTask_source_code = """
from taskforce_common import Task

class TestTask(Task):

    def getName(self):
        return self.name
"""

TestTaskWithParams_source_code = """
from taskforce_common import Task

class TestTaskWithParams(Task):

    parameters = {
        "bool_param": True,
        "int_param" : 123,
        "str_param" : "hello!"
    }

"""

if TEST_FOLDER not in sys.path:
    sys.path += [TEST_FOLDER] + sys.path

CowTime_source_code = getSourceCode('task_library.Cowtime')
EventCounter_source_code = getSourceCode('task_library.EventCounter')
ClockTask_source_code = getSourceCode('task_library.EventCounter')


class TestEngineInterface(unittest.TestCase):

    def setUp(self):
        self.commandConnection = QueueReqRep()
        self.client = self.commandConnection.getClient()
        self.server = self.commandConnection.getServer()
        self.engine = Engine([self.server], [], libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        self.engine.startup()

        self.engineInterface = EngineInterface(self.client)

        self.fileMap = FileMap(['(.+\.py$)|(.+\.group$)'])
        self.fileMap.addFolder(TASK_LIBRARY_PATH)
        self.fileMap.addFolder(BLOCK_LIBRARY_PATH)

    def tearDown(self):
        self.engine.shutdown()
        if os.path.exists(ENGINE_LOCAL_LIBRARY_PATH):
            shutil.rmtree(ENGINE_LOCAL_LIBRARY_PATH)

    def test_groupDeploy(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())

        self.assertTrue(self.engineInterface.groupDeploy(definition))
        status = self.engine.taskStatus()
        self.assertEqual(3, len(status))

    def test_engineShutdown(self):
        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())
        self.assertTrue(self.engineInterface.groupDeploy(definition))

        self.assertTrue(self.engineInterface.engineShutdown())
        self.assertEqual(0, len(self.engine.blocks))
        self.assertFalse(self.engine.eventLoopRunning)
        self.assertFalse(self.engine.commandLoopRunning)
        self.assertFalse(self.engine.statusLoopRunning)

    # def test_engineSerialize(self):
    #     self.fail("Not implemented yet")

    def test_engineSetLogLevel(self):
        level = self.engineInterface.engineSetLogLevel("WARNING")
        self.assertEqual(logging.WARN, self.engine.logger.level)

    def test_engineSetStatusRate(self):
        rate = self.engineInterface.engineSetStatusRate(20)
        self.assertEqual(20, self.engine.statusRate)

    def test_engineStatus(self):
        status = self.engineInterface.engineStatus()
        self.assertNotEquals(ExceptionMessage, type(status))

    def test_libraryAddModule(self):
        self.engineInterface.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = self.engine.libraryGetInfo()
        self.assertEquals('test_library/TestTask.py', info['test_library.TestTask']['rel_path'])

    def test_libraryAddModuleFile(self):
        self.engineInterface.libraryAddModuleFile("task_library.faketask", os.path.join(TASK_LIBRARY_PATH, 'faketask.py'))
        info = self.engine.libraryGetInfo()
        self.assertEquals('task_library/faketask.py', info['task_library.faketask']['rel_path'])

    def test_libraryClear(self):
        self.engineInterface.libraryAddModuleFile("task_library.faketask",
                                                  os.path.join(TASK_LIBRARY_PATH, 'faketask.py'))
        info = self.engine.libraryGetInfo()
        self.assertEquals('task_library/faketask.py', info['task_library.faketask']['rel_path'])

        self.assertTrue(self.engineInterface.libraryClear())
        self.assertFalse(os.path.exists(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'task_library')))

    def test_libraryGetInfo(self):
        self.engineInterface.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = self.engineInterface.libraryGetInfo()["test_library.TestTask"]

        self.assertEqual(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py'), info["path"])
        self.assertEqual('cc9255f27cbf652f2d908d909c03b5f0508f570c', info["hash"])
        self.assertEqual(os.path.join('test_library', 'TestTask.py'), info["rel_path"])

    def test_libraryGetModulePath(self):
        self.engineInterface.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = self.engineInterface.libraryGetModulePath("test_library.TestTask")
        self.assertEqual(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py'), info)

    def test_libraryGetModuleHash(self):
        self.engineInterface.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = self.engineInterface.libraryGetModuleHash("test_library.TestTask")

        self.assertEqual('cc9255f27cbf652f2d908d909c03b5f0508f570c', info)

    def test_libraryGetModuleInfo(self):
        self.engineInterface.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = self.engineInterface.libraryGetModuleInfo("test_library.TestTask")

        self.assertEqual(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py'), info["path"])
        self.assertEqual('cc9255f27cbf652f2d908d909c03b5f0508f570c', info["hash"])
        self.assertEqual(os.path.join('test_library', 'TestTask.py'), info["rel_path"])

    def test_libraryRemoveModule(self):
        self.engineInterface.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = self.engineInterface.libraryGetInfo()["test_library.TestTask"]

        self.assertEqual(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py'), info["path"])
        self.assertEqual('cc9255f27cbf652f2d908d909c03b5f0508f570c', info["hash"])
        self.assertEqual(os.path.join('test_library', 'TestTask.py'), info["rel_path"])

        self.engineInterface.libraryRemoveModule("test_library.TestTask")
        info = self.engineInterface.libraryGetInfo()
        self.assertEqual({}, info)

    def test_libraryRemovePackage(self):
        self.engineInterface.libraryAddModule("test_library.TestTask", TestTask_source_code)
        self.engineInterface.libraryAddModule("test_library.TestTaskWithParams", TestTaskWithParams_source_code)
        info = self.engineInterface.libraryGetInfo()
        self.assertEqual(2, len(info))

        self.engineInterface.libraryRemovePackage("test_library")
        info = self.engineInterface.libraryGetInfo()
        self.assertEqual(0, len(info))

    def test_listTasks(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)
        self.engine.libraryAddModule('task_library.ClockTask', ClockTask_source_code)
        self.engine.libraryAddModule('task_library.EventCounter', EventCounter_source_code)
        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'cowtimer.group'),
                                                          self.fileMap.getMap())

        self.engine.groupDeploy(definition)

        taskList = ["/cowtimer",
                    "/cowtimer/counter",
                    "/cowtimer/clock",
                    "/cowtimer/cow_alarm"]

        self.assertEqual(sorted(taskList), self.engineInterface.listTasks())

    def test_taskDeploy(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)
        taskDef1 = {
            "task": {
                "class": "Cowtime",
                "module": "task_library.Cowtime",
                "name": "task1",
                "parameters": {}
            }
        }

        self.engineInterface.taskDeploy(taskDef1)
        self.assertEqual(1, len(self.engine.blocks))
        self.assertTrue("/task1" in self.engine.blocks)

    def test_taskGetInfo(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())
        self.engine.groupDeploy(definition)

        status = self.engineInterface.taskGetInfo()
        self.assertEqual(3, len(status))

        info = self.engineInterface.taskGetInfo("/two_cows/clementine")["/two_cows/clementine"]
        self.assertEqual("/two_cows/clementine", info["path"])
        self.assertEqual("clementine", info["name"])

    def test_taskSendCommand(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())
        self.engine.groupDeploy(definition)
        self.engineInterface.taskSendCommand('/two_cows', 'startup')

        start_time = time.time()
        timeout = 5 #seconds
        while self.engine.blocks['/two_cows/clementine'].state < TaskState.COMPLETE:
            time.sleep(0.1)
            if time.time() - start_time > timeout:
                break
        self.assertEqual(TaskState.COMPLETE, self.engine.blocks['/two_cows/clementine'].state)

    # def test_taskSetBreakPoint(self):
    #     self.fail("not implemented yet")

    def test_taskSetLogLevel(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())
        self.engine.groupDeploy(definition)

        response = self.engineInterface.taskSetLogLevel("WARNING")
        self.assertEqual(logging.WARNING, self.engine.blocks["/two_cows"].getLogLevel())
        self.assertEqual(logging.WARNING, self.engine.blocks["/two_cows/clara"].getLogLevel())
        self.assertEqual(logging.WARNING, self.engine.blocks["/two_cows/clementine"].getLogLevel())

        response = self.engineInterface.taskSetLogLevel("ERROR", "/two_cows/clementine")
        self.assertEqual(logging.WARNING, self.engine.blocks["/two_cows"].getLogLevel())
        self.assertEqual(logging.WARNING, self.engine.blocks["/two_cows/clara"].getLogLevel())
        self.assertEqual(logging.ERROR, self.engine.blocks["/two_cows/clementine"].getLogLevel())

    # def test_taskSetEventHandlerLogLevel(self):
    #     self.fail()

    def test_taskShutdown(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())
        self.engine.groupDeploy(definition)

        self.engineInterface.taskShutdown("/two_cows/clementine")
        self.assertEqual(2, len(self.engine.blocks))
        self.assertFalse("/two_cows/clementine" in self.engine.blocks)

        self.engineInterface.taskShutdown("/two_cows", shutdownChildren=True)
        self.assertEqual(0, len(self.engine.blocks))
        self.assertEqual(0, len(self.engine.subscriptions))

    def test_taskStartup(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())
        self.engine.groupDeploy(definition)

        self.engineInterface.taskStartup("/two_cows")
        start_time = time.time()
        timeout = 5.0 #seconds
        while self.engine.blocks["/two_cows/clementine"].state < TaskState.COMPLETE:
            time.sleep(0.1)
            if time.time() - start_time > timeout:
                break

        self.assertEqual(TaskState.COMPLETE, self.engine.blocks["/two_cows/clementine"].state)

    def test_taskStatus(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())
        self.engine.groupDeploy(definition)

        status = self.engineInterface.taskStatus()
        self.assertTrue(3, len(status))

        status = self.engineInterface.taskStatus("/two_cows/clara")["/two_cows/clara"]
        self.assertEqual('DEPLOYED', status["state"])

    def test_taskStop(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)
        self.engine.libraryAddModule('task_library.ClockTask', ClockTask_source_code)
        self.engine.libraryAddModule('task_library.EventCounter', EventCounter_source_code)
        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'cowtimer.group'),
                                                          self.fileMap.getMap())

        self.engine.groupDeploy(definition)
        self.engine.taskStartup("/cowtimer/clock")
        while self.engine.blocks["/cowtimer/clock"].state < TaskState.RUNNING:
            time.sleep(0.1)

        self.engineInterface.taskStop("/cowtimer/clock")

        start_time = time.time()
        timeout = 5.0  # seconds
        while self.engine.blocks["/cowtimer/clock"].state != TaskState.STOPPED:
            time.sleep(0.1)
            if time.time() - start_time > timeout:
                break

        self.assertEqual(TaskState.STOPPED, self.engine.blocks["/cowtimer/clock"].state)

    def test_taskGetSetParameter(self):
        self.engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)
        self.engine.libraryAddModule('task_library.ClockTask', ClockTask_source_code)
        self.engine.libraryAddModule('task_library.EventCounter', EventCounter_source_code)
        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'cowtimer.group'),
                                                          self.fileMap.getMap())

        self.engine.groupDeploy(definition)
        self.engine.taskStartup("/cowtimer/clock")
        while self.engine.blocks["/cowtimer/clock"].state < TaskState.RUNNING:
            time.sleep(0.1)

        parameters = self.engineInterface.taskGetParameter("/cowtimer/counter")
        self.assertEqual(10, parameters["goal"])

        self.engineInterface.taskSetParameter("/cowtimer/counter", {"goal": 20})
        parameters = self.engineInterface.taskGetParameter("/cowtimer/counter")
        self.assertEqual(20, parameters["goal"])
