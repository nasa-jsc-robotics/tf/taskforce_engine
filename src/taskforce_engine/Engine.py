import logging
from threading import RLock, Thread
import time
from collections import defaultdict

from taskforce_common import CommandHandler
from taskforce_common import Event, EventHandler, Message, Subscription
from taskforce_common import PipePubSub
from taskforce_common import QueueReqRep
from taskforce_common import TaskFactory, GroupFactory
from taskforce_common.utils import getFunctionDefinitionString
from taskforce_library import LibraryManager

logger = logging.getLogger(__name__)

TaskEngineName = "TaskEngine"


class Engine(object):
    """
    The Engine is the core of the run-time application for TaskForce.  Through it's commandPort, a remote application
    can send commands to the Engine, which in turn, will send commands to one or more tasks.  Status for the Engine and
    Tasks will be published on status ports.
    """

    commands = ['groupDeploy',
                'libraryAddModule',
                'libraryClear',
                'libraryGetModulePath',
                'libraryGetModuleHash',
                'libraryGetModuleInfo',
                'libraryGetInfo',
                'libraryRemoveModule',
                'libraryRemovePackage',
                'listTasks',
                'setStatusRate',
                'taskDeploy',
                'taskGetParameter',
                'taskInit',
                'taskSendCommand',
                'taskSetLogLevel',
                'taskSetParameter',
                'taskShutdown',
                'taskShutdownAll',
                'taskStartup',
                'taskStatus',
                'taskStop'
                ]

    commandConnectionType = QueueReqRep
    eventConnectionType = PipePubSub

    events = ['emitStatus']

    DEFAULT_STATUS_RATE = 1  # Hz
    STATUS_THREAD_JOIN_TIMEOUT = 5  # sec

    def __init__(self, commandPorts, statusPorts, libraryPath=None):
        """
        Args:
            commandPorts: list of Ports used to send commands to the Engine
            statusPorts: list of Ports used by the engine to publish status
            libraryPath: Path used by the LibraryManager to manage the Engine's local Python library.
        """
        self.name = TaskEngineName
        self.logger = logger

        self.eventConnection = Engine.eventConnectionType(poll_timeout=None)
        self.eventPublisher = self.eventConnection.getPublisher()
        self.commandConnection = Engine.commandConnectionType()
        self.commandClient = self.commandConnection.getClient()

        self.eventHandler = EventHandler(self.name)
        self.eventHandler.logger = self.logger
        self.eventHandler.addPort(self.eventConnection.getSubscriber())

        self.commandHandler = CommandHandler(self.name)
        self.commandHandler.logger = self.logger

        for port in commandPorts:
            self.commandHandler.addPort(port)

        self.commandHandler.registerDestination(TaskEngineName, self)

        self._taskStatus = {}            # internal variable to keep up with the status of Tasks

        # initialize factories
        self.groupFactory = GroupFactory()
        self.taskFactory = TaskFactory()

        # dictionary of names(str) : Task/Group
        self.blocks = {}

        # dictionary of name of block(str) : list of (event, callback)
        # This is a map of subscriptions that a particular block has.
        self.subscriptions = defaultdict(set)

        self.statusThread = None
        self.statusRate = Engine.DEFAULT_STATUS_RATE
        self.statusSleepTime = 1. / Engine.DEFAULT_STATUS_RATE

        self.statusPorts = statusPorts
        self.library = LibraryManager(libraryPath)
        self.library.addLibraryToPath()

        self.commandDefinitions = None
        self.registeredCommands = {}
        for command in self.commands:
            self.registerCommand(command)

        self.updateCommandDefinitions()

        self.running = False

        self.lock = RLock()

    @property
    def module(self):
        return self.__module__

    @property
    def className(self):
        return self.__class__.__name__

    @property
    def commandLoopRunning(self):
        """Is the command loop running?"""
        return self.commandHandler.commandLoopRunning

    @property
    def eventLoopRunning(self):
        """Is the event loop running?"""
        return self.eventHandler.eventLoopRunning

    @property
    def statusLoopRunning(self):
        return self.running

    def _registerGroup(self, group):
        """Register all the Tasks and Subgroups within a Group to the EventHandler, CommandHandler, and the engine

        Args:
            group: Group object
        """
        blocks = group.getChildren()

        # add the overall group to the list of blocks
        blocks[group.path] = group

        # register all the blocks
        for block in blocks.values():
            self._registerBlock(block)

        # Setup the subscriptions
        for subscription in group.getSubscriptions():
            self.taskSubscribeEvent(subscription.eventName, subscription.source, subscription.destination,
                                    subscription.callback, subscription.breakpoint)

    def _unregisterGroup(self, group):
        """Disconnect all the Tasks and Subgroups within a Task to the EventHandler and CommandHandlers

        Args:
            group: Group object
        """
        # unregister all child objects
        for child in group.getChildren().values():
            self._unregisterBlock(child)

        self._unregisterBlock(group)

    def _registerBlock(self, block):
        """Register the Block with the Engine

        Args:
            block: A Block object - either a Task or Group object
        """

        if block.path in self.blocks:
            self._unregisterBlock(block)

        self.commandHandler.registerDestination(block.path, block)
        self.eventHandler.registerDestination(block.path, block)
        if hasattr(block, 'startup'):
            self.eventHandler.subscribeEvent('startup', TaskEngineName, block.path, 'startup', False)
            self.eventHandler.subscribeEvent('init', TaskEngineName, block.path, 'init', False)
        block.addEventPort(self.eventPublisher)

        self.blocks[block.path] = block

    def _unregisterBlock(self, block):
        """Register an object's commands with the CommandHandler

        Args:
            block: Either a Task or TaskGroup object
        """
        subscriptions = list(self.subscriptions.get(block.path, []))
        for subscription in subscriptions:
            self.taskUnsubscribeEvent(subscription.eventName, subscription.source, subscription.destination)

        self.commandHandler.unregisterDestination(block.path)
        self.eventHandler.unregisterDestination(block.path)

        self.blocks.pop(block.path)

    def taskNames(self):
        """Return the list of current task names

        Returns: list of strings
        """
        return sorted(self.blocks.keys())

    def startup(self):
        """Start the task engine loops"""
        self.commandHandler.startCommandLoop()
        self.eventHandler.startEventLoop()
        self.startStatusLoop()

    def shutdown(self):
        """Shutdown the task engine loops"""
        self.eventHandler.stopEventLoop()
        self.commandHandler.stopCommandLoop()
        self.stopStatusLoop()
        self.taskShutdownAll()
        return True

    def startStatusLoop(self):
        """Start the status loop

        Returns:
            True
        """
        self.running = True
        if self.statusThread is None:
            self.statusThread = Thread(target=self.statusLoop)
            self.statusThread.daemon = True
            self.statusThread.start()
        return True

    def stopStatusLoop(self):
        """Stop the status loop"""
        self.running = False
        if self.statusThread is not None:
            if self.statusThread.is_alive():
                self.statusThread.join(Engine.STATUS_THREAD_JOIN_TIMEOUT)
                self.statusThread = None

    def status(self, keyword=None):
        """Get the Engine status

        Args:
            keyword: keyword Status element as a string.  If none, all status will be returned

        Returns:
            Status as a dictionary
        """
        status = {}
        status['name'] = self.name
        status['class'] = self.className
        status['module'] = self.module
        status['commandNames'] = self.getCommandNames()
        status['logLevel'] = self.logger.level
        status['commands'] = self.commandDefinitions
        status['statusRate'] = self.statusRate

        if keyword is None:
            return status
        else:
            return status[keyword]

    def breakpointStatus(self):
        """
        Get the status of active breakpoints

        Returns:
            Set of all waiting breakpoints
        """
        return self.eventHandler.getWaitingBreakpoints()

    def statusLoop(self):
        """Status Loop

        The Engine's status loop gathers information about the tasks and breakpoints and sends the data out on all of the engine's
        status ports
        """

        while self.running:
            start_time = time.time()
            status = self.taskStatus()
            breakpointStatus = self.breakpointStatus()
            status['waiting_breakpoints'] = list(breakpointStatus)
            message = Message('status', source=self.name, data=status)
            for port in self.statusPorts:
                port.send(message)

            sleepTime = self.statusSleepTime - (time.time() - start_time)
            if sleepTime > 0:
                time.sleep(sleepTime)

    def getCommandNames(self):
        """Get the name of all registered commands"""
        return sorted(self.registeredCommands.keys())

    def getSubscriptions(self):
        """Get the subscriptions"""
        return self.subscriptions

    def registerCommand(self, name):
        """Register a method as a command.

        Args:
            name: name of the method to register as a command as a string.
        """
        if not hasattr(self, name):
            raise Exception('Trying to create command:"{}" with no matching member method'.format(name))

        callback = getattr(self, name)
        self.registeredCommands[name] = callback

    def updateCommandDefinitions(self):
        """Update the command definitions for the engine."""
        commandDefinitions = []
        for commandName in self.getCommandNames():
            commandDefinitions.append(getFunctionDefinitionString(self.registeredCommands[commandName]))
        self.commandDefinitions = commandDefinitions

    def setLogLevel(self, level):
        """Set the log level for the Engine

        Args:
            level: Log level as a string or value as defined in Python's logging module

        Returns:
            The new log level as a value as defined by Python's logging module
        """
        self.logger.setLevel(level)
        return self.logger.level

    def getLogLevel(self):
        """Get the log level for the Engine

        Returns:
            The log level as a value as defined by Python's logging module
        """
        return self.logger.level

    ##################
    #  API methods

    def addBreakpoint(self, eventName, source, destination, callback):
        return self.eventHandler.subscribeEvent(eventName, source, destination, callback, True)

    def continueBreakpoint(self, eventName, source, destination, args=(), kwargs={}):
        kwargs['continue_breakpoint'] = True
        self.eventPublisher.send(Event(eventName, source, destination, args, kwargs))

    def groupDeploy(self, definition):
        """Create and deploy a group using a JSON definition

        Args:
            definition: JSON definition of a Group

        Returns:
            True on success
        """
        group = self.groupFactory.buildDefinition(definition)

        blocks = group.getChildren()
        blocks[group.path] = group

        with self.lock:
            self._registerGroup(group)

            for block in blocks.values():
                block.deploy()

        return group.path

    def libraryAddModule(self, module_name, source_code, force_reload=False):
        """Add a Python module to the local Python library.

        Args:
            module_name: Name of the module.  Example: taskforce_library.sub_packge.MyModule
            source_code: Contents of the module as a string.
            force_reload: Force the library manager to refresh it's cache and reload the module if it already existed.

        Returns: True
        """
        self.library.addModule(module_name, source_code, force_reload)
        return True

    def libraryClear(self):
        """Remove all code from the local Python library.

        Returns:
            True on success
        """
        self.library.clearLibrary()
        return True

    def libraryGetInfo(self):
        """Get info about the local library

        Returns:
            Python dictionary with library info
        """
        return self.library.getLibraryInfo()

    def libraryGetModulePath(self, module_name):
        """Return the filepath of a module

        Args:
            module_name: name of the module as a string

        Returns: Path to the module as a string
        """
        return self.library.getModulePath(module_name)

    def libraryGetModuleHash(self, module_name):
        """Return the hash of a library module"""
        return self.library.getModuleHash(module_name)

    def libraryGetModuleInfo(self, module_name):
        """Get info about a module in the library"""
        return self.library.getModuleInfo(module_name)

    def libraryRemoveModule(self, module_name):
        """Remove a module from the library"""
        return self.library.removeModule(module_name)

    def libraryRemovePackage(self, package_name):
        """Remove a package from the library"""
        return self.library.removePackage(package_name)

    def listTasks(self):
        """Get a list of deployed Tasks

        Returns:
            Task paths as a list of strings
        """
        return self.taskNames()

    def setStatusRate(self, rate):
        """Set the update rate of status polling.

        Args:
            rate: rate in Hz

        Returns:
            The new status rate in Hz
        """
        if rate <= 0:
            raise Exception("Trying to set status rate at or below zero")

        self.statusRate = rate
        self.statusSleepTime = 1.0 / self.statusRate
        if self.statusThread is not None:
            if self.statusThread.is_alive():
                self.stopStatusLoop()

            self.startStatusLoop()
        return self.statusRate

    def taskDeploy(self, definition):
        """Deploy a single Task to the Engine

        Args:
            definition: JSON task definition

        Returns:
            True on success
        """
        task = self.taskFactory.createTaskFromDefinition(definition)
        self._registerBlock(task)
        task.deploy()
        return True

    def taskGetParameter(self, taskName, key=None):
        """Get a parameter value from a Task

        Args:
            taskName: path of the task as a string
            key: name of the parameter.  If None, return all parameters

        Returns:
            Dictionary of parameters
        """
        return self.taskSendCommand(taskName, 'getParameter', key)

    def taskGetInfo(self, taskName=None, key=None):
        """Get the info of a Task

        Args:
            taskName: Name of the Task as a string.  If None, the info of all tasks will be returned.
            key: Keyword of info to retrieve.  If None, return all info.

        Returns:
            Info as a dictionary
        """
        with self.lock:
            info = {}

            if taskName is not None:
                info = {taskName: self.blocks[taskName].getInfo(key)}
            else:
                for block in self.blocks.values():
                    path = block.path
                    info[path] = block.getInfo(key)
            return info

    def taskGetLogLevel(self, taskName=None):
        response = {}

        if taskName is None:
            tasks = self.taskNames()
        else:
            tasks = [taskName]

        for task in tasks:
            response[task] = self.taskSendCommand(task, 'getLogLevel')

        return response

    def taskInit(self, taskName):
        self.eventPublisher.send(Event('init', TaskEngineName, taskName))
        return True

    def taskSendCommand(self, taskName, command, *args, **kwargs):
        """Send a command to a task

        Args:
            taskName: Path of the task as a string
            command: name of a registered command as a string
            *args: arguments to the command
            **kwargs: key-word arguments to the command

        Returns:
            Return value of the callback
        """
        try:
            callback = getattr(self.blocks[taskName], command)
            response = callback(*args, **kwargs)
        except KeyError:
            raise Exception('Trying to send command to task "{}", but that task does not exist'.format(taskName))
        except AttributeError:
            raise Exception('Trying to send command to invalid member method:{}.{}'.format(taskName, command))
        return response

    def taskSetParameter(self, taskName, keyValues={}):
        return self.taskSendCommand(taskName, 'setParameter', keyValues)

    def taskSetLogLevel(self, level, taskName=None):
        response = {}

        if taskName is None:
            tasks = self.taskNames()
        else:
            tasks = [taskName]

        for task in tasks:
            response[task] = self.taskSendCommand(task, 'setLogLevel', level)

        return response

    def taskShutdown(self, taskName, shutdownChildren=True):
        if shutdownChildren:
            for name in self.blocks.keys():
                if name.startswith(taskName):
                    self.blocks[name].shutdown()
                    self._unregisterBlock(self.blocks[name])
        else:
            self.blocks[taskName].shutdown()
            self._unregisterBlock(self.blocks[taskName])

        return True

    def taskShutdownAll(self):
        """
        Shutdown all tasks
        """
        for blockName in self.blocks.keys():
            self.taskShutdown(blockName, shutdownChildren=False)

    def taskStartup(self, taskName):
        self.eventPublisher.send(Event('startup', TaskEngineName, taskName))
        return True

    def taskStatus(self, taskName=None):
        """
        Get the status of a Task

        Args:
            taskName: Name of the Task as a string.  If None, the status of all tasks will be returned.

        Returns:
            Status as a dictionary
        """
        with self.lock:
            status = {}

            if taskName is not None:
                status = {taskName: self.blocks[taskName].status()}
            else:
                for block in self.blocks.values():
                    path = block.path
                    status[path] = block.status()
            return status

    def taskStop(self, taskName):
        return self.taskSendCommand(taskName, 'stop')

    def taskSubscribeEvent(self, eventName, source, destination, callback, breakpoint):
        """Subscribe a task to an event

        Args:
            eventName: name of the event as a string
            source: Path of the source of the event as a string
            destination: Path of the destination of the event as a string
            callback: method to call in the destination as a string
            breakpoint: Whether or not the subscription has a breakpoint
        """
        subscription = Subscription(eventName, source, destination, callback, breakpoint)
        self.subscriptions[subscription.destination].add(subscription)
        self.eventHandler.subscribeEvent(eventName, source, destination, callback, breakpoint)
        return True

    def taskUnsubscribeEvent(self, eventName, source, destination, callback=None):
        subscriptions = list(self.subscriptions.get(destination, []))
        for subscription in subscriptions:
            if subscription.eventName == eventName and subscription.source == source:
                if subscription.callback == callback or callback is None:
                    self.subscriptions[destination].remove(subscription)
                    if len(self.subscriptions[destination]) == 0:
                        self.subscriptions.pop(destination)
        self.eventHandler.unsubscribeEvent(eventName, source, destination, callback)
        return True

    def removeBreakpoint(self, eventName, source, destination, callback):
        return self.eventHandler.subscribeEvent(eventName, source, destination, callback, False)
