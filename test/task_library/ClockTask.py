import time

from taskforce_common import Task, TaskState, Response, ErrorResponse

class ClockTask(Task):
    events = ['tick','tock']
    commands = ['setRate']

    def __init__(self, name, commandPorts=[]):
        Task.__init__(self,name,commandPorts)
        self.rate = 1 #Hz
        self.sleep_time = 1.0 / self.rate

    def onExecute(self):
        while (self.state == TaskState.RUNNING):
            self.emit('tick')
            time.sleep(self.sleep_time)
            self.emit('tock')

        return True

    def setRate(self, rate):
        self.rate = rate
        self.sleep_time = 1.0 / float(self.rate)
        return self.rate
