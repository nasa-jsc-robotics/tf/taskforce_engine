Change Log
==========

2.0.3
-----

* Change license name in package description
* Add ability to call a nonblocking task init
* Add functions to continue, add, and delete a breakpoint

2.0.2
-----

* Added license file

2.0.1
-----

* Fixed unittest

2.0.0
-----

* Modified the engine to be compatible with taskforce_common: updated classes, definitions, schemas etc. 
* Architectural change so that the Engine owns the only CommandHandler and EventHandler
* Added groupDeployFile to task_engine_cli, allowing simple deployment of Group files
* Added profiling to the task_engine
* Enabled engineSerialize in the EngineInterface
* `Engine.py` returns the group path string as opposed to a boolean value
* Removed trusty CI

1.1.1
-----

* Added xenial CI
