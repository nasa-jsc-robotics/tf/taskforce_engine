import cmd
import inspect
from pprint import pprint
import readline
import sys

from taskforce_common import ExceptionMessage
from taskforce_common import GroupDefinitionLoader
from taskforce_common.utils import FileMap
from .EngineInterface import EngineInterface

readline.set_completer_delims(' \t\n...@#$%^&*()-=+[{]}\\|;:\'",<>?')


def getDefString(obj):
    """Return a string of a Python function or member method definition.

    This method is used by the CLI to format the 'help' printed to the screen

    Args:
        obj: Python callable

    Returns:
        String showing the function/method definition
    """
    if not callable(obj):
        raise Exception('{} is not callable'.format(obj))
    sourceLines = inspect.getsourcelines(obj)[0]
    raw_def = sourceLines[0]
    def_line = raw_def.strip()
    if def_line.startswith('def'):
        def_line = def_line[3:]
    def_line = def_line.strip().rstrip(':')
    def_line = def_line.replace("self, ", "")
    def_line = def_line.replace("self,", "")
    def_line = def_line.replace("self", "")

    return def_line


class EngineCLI(cmd.Cmd):
    """Convenience class to create a command-line interface to interact with the Engine."""

    prompt     = '? for help > '
    help_width = 61

    def __init__(self, clientPort, path="./"):
        """
        @type clientPortDict dictionary
        @param clientPortDict Dictionary of tasknames : clientPort objects
        """
        cmd.Cmd.__init__(self)
        self.engineInterface = EngineInterface(clientPort)
        self.fileMap = FileMap(['(.+\.py$)|(.+\.group$)'])
        self.fileMap.addFolder(path)

    def cmdloop(self, intro=None):
        try:
            cmd.Cmd.cmdloop(self,intro)
        except Exception as e:
            print e
            self.cmdloop(self)

    def getArgs(self, line):
        _args = [x.strip() for x in line.split(' ')]
        args = [x for x in _args if x != '']
        return args

    def formatHelp(self, function, helpString):
        return '{:<{width}} - {}'.format(getDefString(function),helpString, width=self.help_width)

    def printResult(self, result):
        if isinstance(result, dict):
            for key, value in result.items():
                print '{}:'.format(key)
                self._printResult(value)
        else:
            self._printResult(result)

    def _printResult(self, result):
        if isinstance(result, ExceptionMessage):
            print 'Command returned an exception:'
            print result.traceback
        pprint(result)

    def emptyline(self):
        pass

    ################################
    # Menu items:
    ################################

    #---------
    # intro
    #---------

    def do_intro(self, line):
        print self.intro

    def help_intro(self):
        print '{:<{width}} - {}'.format('intro','Show intro',width=self.help_width)

    #---------
    # help
    #---------

    def do_help(self, line):
        cmd.Cmd.do_help(self,line)
        self.help_help()

    def help_help(self):
        members = dir(self)
        for m in members:
            if m.startswith('help_'):
                if m != 'help_help':
                    attr = getattr(self, m)
                    if callable(attr):
                        attr()

    #---------
    # quit
    #---------
    def do_quit(self, line):
        """Quit the application """
        try:
            self.do_shutdown(line)
        except:
            pass

        sys.exit(0)

    def help_quit(self):
        print '{:<{width}} - {}'.format('quit','Shutdown tasks and quit',width=self.help_width)

    #---------
    # API
    #---------
    def do_groupDeploy(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.groupDeploy(*args))

    def help_groupDeploy(self):
        print self.formatHelp(self.engineInterface.groupDeploy, "Deploy a group to the engine")

    def do_groupDeployFile(self, line):
        args = self.getArgs(line)
        definition = GroupDefinitionLoader.loadDefinition(args[0], self.fileMap.getMap())
        print definition
        self.printResult(self.engineInterface.groupDeploy(definition))

    def help_groupDeployFile(self):
        print '{:<{width}} - {}'.format("groupDeployFile(file_name)", "Deploy a group to the engine by file", width=self.help_width)

    def do_engineSetLogLevel(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.engineSetLogLevel(*args))

    def help_engineSetLogLevel(self):
        print self.formatHelp(self.engineInterface.engineSetLogLevel, "Set the log level for the engine")

    def do_engineSetStatusRate(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.engineSetStatusRate(*args))

    def help_engineSetStatusRate(self):
        print self.formatHelp(self.engineInterface.engineSetStatusRate,"Set the status publish rate of the engine")

    # def do_engineSerialize(self, line):
    #     args = self.getArgs(line)
    #     self.printResult(self.engineInterface.engineSerialize(*args))
    #
    # def help_engineSerialize(self):
    #     print self.formatHelp(self.engineInterface.engineengineSerialize,"Serialize the current deployment state")

    def do_engineStatus(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.engineStatus(*args))

    def help_engineStatus(self):
        print self.formatHelp(self.engineInterface.engineStatus, "Get the status of the Engine")

    def do_libraryAddModule(self, line):
        """def libraryAddModule(self, package_name, module_name, source_code=None)"""
        args = self.getArgs(line)
        self.printResult(self.engineInterface.libraryAddModule(*args))

    def help_libraryAddModule(self):
        print self.formatHelp(self.engineInterface.libraryAddModule,
                              "Add a python module to the library")

    def do_libraryAddModuleFile(self, line):
        """def libraryAddModule(self, package_name, module_name, source_code=None)"""
        args = self.getArgs(line)
        self.printResult(self.engineInterface.libraryAddModuleFile(*args))

    def help_libraryAddModuleFile(self):
        print self.formatHelp(self.engineInterface.libraryAddModuleFile,
                              "Add a python module to the library by filename")

    def do_libraryClear(self, line):
        """def libraryClear(self)"""
        args = self.getArgs(line)
        self.printResult(self.engineInterface.libraryClear(*args))

    def help_libraryClear(self):
        print self.formatHelp(self.engineInterface.libraryClear, "Remove all packages and modules from the library")

    def do_libraryGetInfo(self, line):
        """def libraryGetInfo(self)"""
        args = self.getArgs(line)
        self.printResult(self.engineInterface.libraryGetInfo(*args))

    def help_libraryGetInfo(self):
        print self.formatHelp(self.engineInterface.libraryGetInfo, "Print info about the library")

    def do_libraryGetModulePath(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.libraryGetModulePath(*args))

    def help_libraryGetModulePath(self):
        print self.formatHelp(self.engineInterface.libraryGetModulePath, "Get the path of a library module")

    def do_libraryGetModuleHash(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.libraryGetModuleHash(*args))

    def help_libraryGetModuleHash(self):
        print self.formatHelp(self.engineInterface.libraryGetModuleHash, "Get the hash of a library module")

    def do_libraryGetModuleInfo(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.libraryGetModuleInfo(*args))

    def help_libraryGetModuleInfo(self):
        print self.formatHelp(self.engineInterface.libraryGetModuleInfo, "Get information about a library module")

    def do_libraryRemoveModule(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.libraryRemoveModule(*args))

    def help_libraryRemoveModule(self):
        print self.formatHelp(self.engineInterface.libraryRemoveModule, "Remove a module from the library")

    def do_libraryRemovePackage(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.libraryRemovePackage(*args))

    def help_libraryRemovePackage(self):
        print self.formatHelp(self.engineInterface.libraryRemovePackage, "Remove a package from the library")

    def do_listTasks(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.listTasks(*args))

    def help_listTasks(self):
        print self.formatHelp(self.engineInterface.listTasks, "List all the deployed Tasks in the Task Engine")

    def do_printFileMap(self, line):
        args = self.getArgs(line)
        filemap = sorted(self.fileMap.getMap().items())
        for k, v in filemap:
            print '{} : {}'.format(k, v)

    def help_printFileMap(self):
        print '{:<{width}} - {}'.format("printFileMap()", "Print out the file map (the user TaskForce library)", width=self.help_width)

    def do_taskDeploy(self, line):
        args = self.getArgs(line)
        definition = eval(args[0])
        print definition
        self.printResult(self.engineInterface.taskDeploy(definition))

    def help_taskDeploy(self):
        print self.formatHelp(self.engineInterface.taskDeploy, "Deploy a Task in the Task Engine")

    def do_taskSendCommand(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.taskSendCommand(*args))

    def help_taskSendCommand(self):
        print self.formatHelp(self.engineInterface.taskSendCommand, "Send a command to a Task in the engine")

    def do_taskSetLogLevel(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.taskSetLogLevel(*args))

    def help_taskSetLogLevel(self):
        print self.formatHelp(self.engineInterface.taskSetLogLevel, "Set the log level of a Task")

    def do_taskShutdown(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.taskShutdown(*args))

    def help_taskShutdown(self):
        print self.formatHelp(self.engineInterface.taskStartup, "Shutdown a Task (un-deploy)")

    def do_taskStartup(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.taskStartup(*args))

    def help_taskStartup(self):
        print self.formatHelp(self.engineInterface.taskStartup, "Start a Task")

    def do_taskStatus(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.taskStatus(*args))

    def help_taskStatus(self):
        print self.formatHelp(self.engineInterface.taskStatus, "Print status of a Task")

    def do_taskStop(self, line):
        args = self.getArgs(line)
        self.printResult(self.engineInterface.taskStop(*args))

    def help_taskStop(self):
        print self.formatHelp(self.engineInterface.taskStop, "Stop a Task")
