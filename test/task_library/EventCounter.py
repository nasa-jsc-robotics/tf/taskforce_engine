# standard imports.  Add more imports as needed.
from taskforce_common import Task, TaskState, Response, ErrorResponse
import time

# Change the name of the task
class EventCounter(Task):
    events = ['goalReached']
    commands = ['resetCount','increaseCount']
    parameters = {'goal' : 10}

    def __init__(self, name, commandPorts=[]):
        Task.__init__(self,name,commandPorts)
        self.count = 0
        self.goal_reached = False

    def onInit(self):
        self.count = 0
        self.goal_reached = False

        return True

    def onStartup(self):
        self.count = 0
        self.goal_reached = False

        return True

    def resetCount(self):
        self.count = 0
        self.goal_reached = False
        return self.count

    def increaseCount(self):
        self.count += 1
        self.logger.info('count = {}'.format(self.count))
        if self.count >= self.parameters['goal']:
            if not self.goal_reached:
                self.goal_reached = True
                self.emit('goalReached')
        return self.count
