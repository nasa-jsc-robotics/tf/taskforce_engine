import unittest
import os
import shutil
import sys

from taskforce_common import QueueReqRep
from taskforce_engine import Engine
from taskforce_engine import EngineManager


TEST_FOLDER = os.path.dirname(os.path.realpath(__file__))
TASK_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'task_library')
BLOCK_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'block_library')
ENGINE_LOCAL_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'engine_local_library_path')


class TestEngineManager(unittest.TestCase):

    def setUp(self):
        self.commandConnection = QueueReqRep()
        self.client = self.commandConnection.getClient()
        self.server = self.commandConnection.getServer()
        self.engine = Engine([self.server], [], libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        self.engine.startup()

    def tearDown(self):
        self.engine.shutdown()
        if os.path.exists(ENGINE_LOCAL_LIBRARY_PATH):
            shutil.rmtree(ENGINE_LOCAL_LIBRARY_PATH)

    def test_addLocalLibraryFolder(self):
        manager = EngineManager(self.client)
        manager.addLocalLibraryFolder(BLOCK_LIBRARY_PATH)
        manager.addLocalLibraryFolder(TASK_LIBRARY_PATH)
        map = manager.fileMap.getMap()
        self.assertEqual(16, len(map.keys()))

        self.assertEqual(1, len(manager.parents))
        for parent in manager.parents:
            self.assertTrue(parent in sys.path)

    def test_removeLocalLibraryFolder(self):
        manager = EngineManager(self.client)
        manager.addLocalLibraryFolder(BLOCK_LIBRARY_PATH)
        manager.addLocalLibraryFolder(TASK_LIBRARY_PATH)
        self.assertEqual(2, len(manager.parents[TEST_FOLDER]))

        manager.removeLocalLibraryFolder(BLOCK_LIBRARY_PATH)
        map = manager.fileMap.getMap()
        self.assertEqual(8, len(map.keys()))

        parent = TEST_FOLDER
        self.assertEqual(1, len(manager.parents[TEST_FOLDER]))

        manager.removeLocalLibraryFolder(TASK_LIBRARY_PATH)
        self.assertEqual(0, len(manager.parents))
        self.assertFalse(parent in sys.path)

    def test_libraryAddModuleFromGroup(self):
        manager = EngineManager(self.client)
        manager.addLocalLibraryFolder(BLOCK_LIBRARY_PATH)
        manager.addLocalLibraryFolder(TASK_LIBRARY_PATH)
        manager.libraryAddModuleFromGroup("block_library/cowtimer.group")

        libraryInfo = self.engine.libraryGetInfo()
        self.assertEqual(3, len(libraryInfo))
        self.assertTrue("task_library.Cowtime" in libraryInfo)
        self.assertTrue("task_library.ClockTask" in libraryInfo)
        self.assertTrue("task_library.EventCounter" in libraryInfo)

    def test_libraryIsModuleLoadNeeded(self):
        manager = EngineManager(self.client)

        # should raise an exception because there is no module called "some.Module"
        self.assertRaises(ImportError, manager.libraryModuleMatchesRemote, "some.Module")

        manager.addLocalLibraryFolder(TASK_LIBRARY_PATH)
        self.assertFalse(manager.libraryModuleMatchesRemote("task_library.Cowtime"))
        manager.libraryAddModule("task_library.Cowtime")
        self.assertTrue(manager.libraryModuleMatchesRemote("task_library.Cowtime"))

    def test_groupDeployByName(self):
        manager = EngineManager(self.client)

        manager.addLocalLibraryFolder(TASK_LIBRARY_PATH)
        manager.addLocalLibraryFolder(BLOCK_LIBRARY_PATH)

        self.assertEqual(0, len(self.engine.libraryGetInfo()))

        manager.groupDeployByName("block_library/cowtimer.group")

        libraryInfo = self.engine.libraryGetInfo()

        self.assertEqual(3, len(libraryInfo))
        self.assertTrue("task_library.Cowtime" in libraryInfo)
        self.assertTrue("task_library.ClockTask" in libraryInfo)
        self.assertTrue("task_library.EventCounter" in libraryInfo)
        self.assertEqual(4, len(self.engine.blocks))

    def test_taskDeploySimple(self):
        manager = EngineManager(self.client)

        manager.addLocalLibraryFolder(TASK_LIBRARY_PATH)

        manager.taskDeploySimple("mable", "task_library.Cowtime", "Cowtime")
        self.assertTrue(1, len(self.engine.blocks))
        self.assertTrue("/mable" in self.engine.taskStatus())

        # test simple deploy with a task that has parameters
        manager.taskDeploySimple("counter", "task_library.EventCounter", "EventCounter")

        self.assertTrue(2, len(self.engine.blocks))
        self.assertTrue("/counter" in self.engine.taskStatus())
        self.assertEqual({"goal": 10}, self.engine.taskGetParameter("/counter", "goal"))
