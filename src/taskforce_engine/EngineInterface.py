#!/usr/bin/env python
from taskforce_common.utils import getSourceCode
from taskforce_common import CommandInterface
from .Engine import TaskEngineName


class EngineInterface(object):

    def __init__(self, clientPort):
        self.client = clientPort
        self.commandInterface = CommandInterface('remote', self.client)

    def _sendCommand(self, commandName, destination, *args, **kwargs):
        return self.commandInterface.sendCommand(commandName, destination, *args, **kwargs)

    def addBreakpoint(self, eventName, source, destination, callback):
        return self._sendCommand('addBreakpoint', TaskEngineName, eventName, source, destination, callback)

    def continueBreakpoint(self, eventName, source, destination, args=(), kwargs={}):
        return self._sendCommand('continueBreakpoint', TaskEngineName, eventName, source, destination, args, kwargs)

    def groupDeploy(self, definition):
        return self._sendCommand('groupDeploy', TaskEngineName, definition)

    def engineShutdown(self):
        return self._sendCommand('shutdown', TaskEngineName)

    def engineSetLogLevel(self, level):
        return self._sendCommand('setLogLevel', TaskEngineName, level)

    def engineSetStatusRate(self, rate):
        return self._sendCommand('setStatusRate', TaskEngineName, rate)

    def engineStatus(self):
        return self._sendCommand('status', TaskEngineName)

    def libraryAddModule(self, module_name, source_code=None, force_reload=False):
        if source_code is None:
            source_code = getSourceCode(module_name)
        return self._sendCommand('libraryAddModule', TaskEngineName, module_name, source_code, force_reload)

    def libraryAddModuleFile(self, module_name, file_path, force_reload=False):
        with open(file_path, 'r') as file:
            source_code = file.read()
        return self._sendCommand('libraryAddModule', TaskEngineName, module_name, source_code, force_reload)

    def libraryClear(self):
        return self._sendCommand('libraryClear', TaskEngineName)

    def libraryGetInfo(self):
        return self._sendCommand('libraryGetInfo', TaskEngineName)

    def libraryGetModulePath(self, module_name):
        return self._sendCommand('libraryGetModulePath', TaskEngineName, module_name)

    def libraryGetModuleHash(self, module_name):
        return self._sendCommand('libraryGetModuleHash', TaskEngineName, module_name)

    def libraryGetModuleInfo(self, module_name):
        return self._sendCommand('libraryGetModuleInfo', TaskEngineName, module_name)

    def libraryRemoveModule(self, module_name):
        return self._sendCommand('libraryRemoveModule', TaskEngineName, module_name)

    def libraryRemovePackage(self, package_name):
        return self._sendCommand('libraryRemovePackage', TaskEngineName, package_name)

    def listTasks(self):
        return self._sendCommand('listTasks', TaskEngineName)

    def taskDeploy(self, definition):
        return self._sendCommand('taskDeploy', TaskEngineName, definition)

    def taskGetInfo(self, taskName=None):
        return self._sendCommand('taskGetInfo', TaskEngineName, taskName)

    def taskInit(self, taskName):
        return self._sendCommand('taskInit', TaskEngineName, taskName)

    def taskSendCommand(self, taskName, command, *args, **kwargs):
        return self._sendCommand("taskSendCommand", TaskEngineName, taskName, command, *args, **kwargs)

    def taskSetLogLevel(self, level, taskName=None):
        return self._sendCommand('taskSetLogLevel', TaskEngineName, level, taskName)

    def taskShutdown(self, taskName, shutdownChildren=True):
        return self._sendCommand('taskShutdown', TaskEngineName, taskName, shutdownChildren)

    def taskShutdownAll(self):
        return self._sendCommand('taskShutdownAll', TaskEngineName)

    def taskStartup(self, taskName):
        return self._sendCommand('taskStartup', TaskEngineName, taskName)

    def taskStatus(self, taskName=None):
        return self._sendCommand('taskStatus', TaskEngineName, taskName)

    def taskStop(self, taskName=None):
        return self._sendCommand('taskStop', TaskEngineName, taskName)

    def taskSetParameter(self, taskName, keyValues={}):
        return self._sendCommand('taskSetParameter', TaskEngineName, taskName, keyValues)

    def taskGetParameter(self, taskName, key=None):
        return self._sendCommand('taskGetParameter', TaskEngineName, taskName, key)

    def removeBreakpoint(self, eventName, source, destination, callback):
        return self._sendCommand('removeBreakpoint', TaskEngineName, eventName, source, destination, callback)
