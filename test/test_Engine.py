import unittest
import logging
import os
import shutil
import time
import sys

from taskforce_common import GroupDefinitionLoader, PipeReqRep, PipePubSub, Command, ExceptionMessage, Task
from taskforce_common import TaskState
from taskforce_common.utils import FileMap, getSourceCode
from taskforce_common import BLOCK_FILE_EXTENSION

from taskforce_engine import Engine, TaskEngineName

TEST_FOLDER = os.path.dirname(os.path.realpath(__file__))
TASK_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'task_library')
BLOCK_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'block_library')

ENGINE_LOCAL_LIBRARY_PATH = os.path.join(TEST_FOLDER, 'engine_local_library_path')

GROUP_DEFINITION = {
    "group": {
        "eventRelays": [
            {
                "eventName": "startup",
                "sourceName": "TwoCows"
            },
            {
                "eventName": "complete",
                "sourceName": "TwoCows"
            }
        ],
        "fileName": "block_library/two_cows.group",
        "name": "TwoCows",
        "parameterBindings": [],
        "parameters": {},
        "subGroups": [],
        "subscriptions": [
            {
                "callback": "complete",
                "destination": "TwoCows",
                "eventName": "complete",
                "source": "clementine"
            },
            {
                "callback": "startup",
                "destination": "clementine",
                "eventName": "complete",
                "source": "clara"
            },
            {
                "callback": "startup",
                "destination": "clara",
                "eventName": "startup",
                "source": "TwoCows"
            }
        ],
        "tasks": [
            {
                "task": {
                    "class": "Cowtime",
                    "module": "task_library.Cowtime",
                    "name": "clara",
                    "parameters": {}
                }
            },
            {
                "task": {
                    "class": "Cowtime",
                    "module": "task_library.Cowtime",
                    "name": "clementine",
                    "parameters": {}
                }
            }
        ]
    }
}

hello_world_source_code = """
def hello_world(name):
    print 'Hello, {}!'.format(name)
"""

TestTask_source_code = """
from taskforce_common import Task

class TestTask(Task):

    def getName(self):
        return self.name
"""

TestTaskWithParams_source_code = """
from taskforce_common import Task

class TestTaskWithParams(Task):

    parameters = {
        "bool_param": True,
        "int_param" : 123,
        "str_param" : "hello!"
    }

"""

TestTaskWithLoop_source_code = """
from taskforce_common import Task, TaskState
import time

class TestTaskWithLoop(Task):

    def onExecute(self):
        while self.state == TaskState.RUNNING:
            time.sleep(0.1)
        return True
"""

if TEST_FOLDER not in sys.path:
    sys.path += [TEST_FOLDER] + sys.path

CowTime_source_code = getSourceCode('task_library.Cowtime')


class TestEngine(unittest.TestCase):

    def setUp(self):
        self.cmd_conn = PipeReqRep()
        self.status_conn = PipePubSub()

        self.engineServerPort = self.cmd_conn.getServer()
        self.remoteClientPort = self.cmd_conn.getClient()

        self.engineStatusPort = self.status_conn.getPublisher()
        self.remoteStatusPort = self.status_conn.getSubscriber()

        self.fileMap = FileMap(['(.+\.py$)|(.+\.group$)'])
        self.fileMap.addFolder(TASK_LIBRARY_PATH)
        self.fileMap.addFolder(BLOCK_LIBRARY_PATH)

    def tearDown(self):
        if os.path.exists(ENGINE_LOCAL_LIBRARY_PATH):
            shutil.rmtree(ENGINE_LOCAL_LIBRARY_PATH)

    # @unittest.skip('reasons')
    def test_startup(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)

        engine.startup()
        self.assertTrue(engine.eventLoopRunning)
        self.assertTrue(engine.commandLoopRunning)
        engine.shutdown()

        self.assertFalse(engine.eventLoopRunning)
        self.assertFalse(engine.commandLoopRunning)

    def test_getLogLevel(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)

        engine.setLogLevel('DEBUG')
        self.assertEqual(logging.DEBUG, engine.getLogLevel())
        engine.setLogLevel('WARNING')
        self.assertEqual(logging.WARNING, engine.getLogLevel())

    # @unittest.skip("reasons")
    def test_groupDeploy(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)
        engine.startup()

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())

        tasks = []
        for task in definition['group']['tasks']:
            tasks.append(task['task']['name'])

        engine.groupDeploy(definition)
        status = engine.taskStatus()
        self.assertEqual(3, len(status))

        engine.shutdown()
        self.assertFalse(engine.eventLoopRunning)
        self.assertFalse(engine.commandLoopRunning)

    # @unittest.skip("reasons")
    def test_groupDeploy_2_levels(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)
        engine.startup()

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'herd.group'),
                                                          self.fileMap.getMap())

        engine.groupDeploy(definition)
        status = engine.taskStatus()
        self.assertEqual(7, len(status))

        engine.shutdown()
        self.assertFalse(engine.eventLoopRunning)
        self.assertFalse(engine.commandLoopRunning)

    # @unittest.skip("failing this test")
    def test_groupDeploy_existing_tasks(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)

        definition = {
            "version": 2.0,
            "group": {
                "eventRelays": [
                    {
                        "eventName": "shutdown",
                        "sourceName": "<group>"
                    }
                ],
                "fileName": "taskforce_example_library/test/RedeployTest" + BLOCK_FILE_EXTENSION,
                "name": "RedeployTest",
                "parameterBindings": [],
                "parameters": {},
                "subGroups": [],
                "subscriptions": [
                    {
                        "callback": "shutdown",
                        "destination": "Task1",
                        "eventName": "shutdown",
                        "source": "<group>"
                    },
                    {
                        "callback": "startup",
                        "destination": "Task2",
                        "eventName": "complete",
                        "source": "Task1"
                    },
                    {
                        "callback": "shutdown",
                        "destination": "Task2",
                        "eventName": "shutdown",
                        "source": "<group>"
                    },
                ],
                "tasks": [
                    {
                        "task": {
                            "class": "Task",
                            "module": "taskforce_common.task.Task",
                            "name": "Task1",
                            "parameters": {}
                        }
                    },
                    {
                        "task": {
                            "class": "Task",
                            "module": "taskforce_common.task.Task",
                            "name": "Task2",
                            "parameters": {}
                        }
                    }
                ]
            }
        }

        N = 50
        for i in range(N):
            engine.groupDeploy(definition)
            status = engine.taskStatus()
            self.assertEqual(3, len(status),
                             '{} expected, but only had {} (failed on iteration {})\nStatus:{}'.format(3, len(status),
                                                                                                       i, status))

        engine.shutdown()

    # @unittest.skip("reasons")
    def test_groupDeploy_exception_handling(self):
        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library/exception.group'),
                                                          self.fileMap.getMap())

        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)
        engine.startup()

        response = self.remoteClientPort.request(Command('groupDeploy', 'test', TaskEngineName, definition))
        self.assertTrue(ExceptionMessage, type(response))

        response = self.remoteClientPort.request(Command('listTasks', 'test', TaskEngineName))
        self.assertEquals(response.data, [])

        engine.shutdown()

    def test_libraryAddModule(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = engine.libraryGetInfo()
        self.assertEquals('test_library/TestTask.py', info['test_library.TestTask']['rel_path'])

    def test_libraryClear(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = engine.libraryGetInfo()
        self.assertEquals('test_library/TestTask.py', info['test_library.TestTask']['rel_path'])

        engine.libraryClear()

        self.assertFalse(os.path.exists(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library')))

    def test_libraryGetInfo(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = engine.libraryGetInfo()["test_library.TestTask"]
        self.assertEqual(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py'), info["path"])
        self.assertEqual('cc9255f27cbf652f2d908d909c03b5f0508f570c', info["hash"])
        self.assertEqual(os.path.join('test_library','TestTask.py'), info["rel_path"])

    def test_libraryGetModulePath(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTask", TestTask_source_code)

        self.assertEqual(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py'),
                         engine.libraryGetModulePath("test_library.TestTask"))

    def test_libraryGetModuleHash(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTask", TestTask_source_code)

        self.assertEqual('cc9255f27cbf652f2d908d909c03b5f0508f570c',
                         engine.libraryGetModuleHash("test_library.TestTask"))

    def test_libraryGetModuleInfo(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTask", TestTask_source_code)
        info = engine.libraryGetModuleInfo("test_library.TestTask")
        self.assertEqual(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py'), info["path"])
        self.assertEqual('cc9255f27cbf652f2d908d909c03b5f0508f570c', info["hash"])
        self.assertEqual(os.path.join('test_library', 'TestTask.py'), info["rel_path"])

    def test_libraryRemoveModule(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTask", TestTask_source_code)
        self.assertTrue(os.path.exists(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py')))

        engine.libraryRemoveModule("test_library.TestTask")

        self.assertTrue(os.path.exists(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library')))
        self.assertFalse(os.path.exists(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py')))

    def test_libraryRemovePackage(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTask", TestTask_source_code)
        self.assertTrue(os.path.exists(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py')))

        engine.libraryRemovePackage("test_library")

        self.assertFalse(os.path.exists(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library')))
        self.assertFalse(os.path.exists(os.path.join(ENGINE_LOCAL_LIBRARY_PATH, 'test_library', 'TestTask.py')))

    def test_registerBlock(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)

        task = Task('Task1')
        engine._registerBlock(task)

        self.assertEqual(1, len(engine.blocks))
        self.assertTrue('/Task1' in engine.blocks)
        self.assertEqual(task, engine.blocks['/Task1'])

        task2 = Task('Task1')
        engine._registerBlock(task2)
        self.assertEqual(1, len(engine.blocks))
        self.assertTrue('/Task1' in engine.blocks)
        self.assertEqual(task2, engine.blocks['/Task1'])

    def test_setLogLevel(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)

        self.assertEqual(logging.DEBUG, engine.setLogLevel('DEBUG'))
        self.assertEqual(logging.DEBUG, engine.logger.level)
        self.assertEqual(logging.WARNING, engine.setLogLevel('WARNING'))
        self.assertEqual(logging.WARNING, engine.logger.level)

    def test_registerCommand(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)

        commandNames = engine.getCommandNames()

        self.assertEqual(len(engine.commands), len(engine.getCommandNames()))

        engine.registerCommand('status')
        self.assertEqual(len(engine.commands) + 1, len(engine.getCommandNames()))
        self.assertRaises(Exception, engine.registerCommand, 'bogusMethod')

    def test_setStatusRate(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)

        self.assertEqual(engine.DEFAULT_STATUS_RATE, engine.status('statusRate'))

        # assert that setting the status rate to zero or negative raises an exception
        self.assertRaises(Exception, engine.setStatusRate, 0)
        self.assertRaises(Exception, engine.setStatusRate, -1)

        engine.setStatusRate(10)
        self.assertEqual(10, engine.status('statusRate'))
        engine.startup()

        engine.setStatusRate(5)
        self.assertTrue(engine.statusLoopRunning)
        self.assertEqual(5, engine.status('statusRate'))

        engine.shutdown()

    def test_startStatusLoop(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'herd.group'),
                                                          self.fileMap.getMap())
        engine.groupDeploy(definition)

        engine.startStatusLoop()

        self.assertTrue(engine.statusLoopRunning)
        self.assertNotEqual(None, engine.statusThread)

        # stop the status loop
        engine.stopStatusLoop()
        self.assertFalse(engine.statusLoopRunning)

        # restart the status loop
        engine.startStatusLoop()
        self.assertTrue(engine.statusLoopRunning)

    def test_taskDeploy(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTask", TestTask_source_code)

        taskDef = {
            "task": {
                "class": "TestTask",
                "module": "test_library.TestTask",
                "name": "task1",
                "parameters": {}
            }
        }

        engine.taskDeploy(taskDef)
        self.assertEqual(1, len(engine.blocks))
        self.assertTrue("/task1" in engine.blocks)

    def test_taskGetInfo(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)

        engine.startup()

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'herd.group'),
                                                          self.fileMap.getMap())
        engine.groupDeploy(definition)

        # get all info
        info = engine.taskGetInfo()

        i = info["/herd/TwoCows01"]
        self.assertEqual("TwoCows01", i["name"])
        self.assertEqual("/herd/TwoCows01", i["path"])
        self.assertTrue("complete" in i["eventNames"])
        engine.shutdown()

    def test_taskGetParameter(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTaskWithParams", TestTaskWithParams_source_code)

        taskDef = {
            "task": {
                "class": "TestTaskWithParams",
                "module": "test_library.TestTaskWithParams",
                "name": "task1",
                "parameters": {
                    "bool_param": False,
                    "int_param": 567,
                    "str_param": "Goodbye"
                }
            }
        }

        engine.taskDeploy(taskDef)
        self.assertEqual(1, len(engine.blocks))
        self.assertTrue("/task1" in engine.blocks)
        self.assertEqual(False, engine.taskGetParameter("/task1")["bool_param"])

    def test_taskGetParameter_group(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTaskWithParams", TestTaskWithParams_source_code)

        groupDef = {
            "group" : {

            }
        }

    def test_taskGetLogLevel(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)

        taskDef1 = {"task": {"class": "Task", "module": "taskforce_common.task.Task", "name": "task1", "parameters": {}}}
        taskDef2 = {"task": {"class": "Task", "module": "taskforce_common.task.Task", "name": "task2", "parameters": {}}}

        engine.taskDeploy(taskDef1)
        engine.taskDeploy(taskDef2)

        level = engine.taskGetLogLevel()
        self.assertEqual(logging.NOTSET, level['/task1'])
        self.assertEqual(logging.NOTSET, level['/task2'])

        self.assertEqual(logging.NOTSET, engine.taskGetLogLevel("/task1")["/task1"])
        self.assertEqual(logging.NOTSET, engine.taskGetLogLevel("/task2")["/task2"])

    def test_taskSendCommand(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)

        taskDef1 = {"task": {"class": "Task", "module": "taskforce_common.task.Task", "name": "task1", "parameters": {}}}
        taskDef2 = {"task": {"class": "Task", "module": "taskforce_common.task.Task", "name": "task2", "parameters": {}}}

        engine.taskDeploy(taskDef1)
        engine.taskDeploy(taskDef2)

        self.assertTrue(engine.taskSendCommand("/task1", "startup"))
        self.assertRaises(Exception, engine.taskSendCommand, "/task3", "startup")
        self.assertRaises(Exception, engine.taskSendCommand, "/task1", "bad_command")

    def test_taskSetParameter(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.libraryAddModule("test_library.TestTaskWithParams", TestTaskWithParams_source_code)

        taskDef = {
            "task": {
                "class": "TestTaskWithParams",
                "module": "test_library.TestTaskWithParams",
                "name": "task1",
                "parameters": {
                    "bool_param": False,
                    "int_param": 567,
                    "str_param": "Goodbye"
                }
            }
        }

        engine.taskDeploy(taskDef)
        self.assertEqual(1, len(engine.blocks))
        self.assertTrue("/task1" in engine.blocks)
        engine.taskSetParameter('/task1', {"int_param" : 999})
        self.assertEqual(999, engine.taskGetParameter("/task1")["int_param"])

    def test_taskSetLogLevel(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)

        taskDef1 = {
            "task": {"class": "Task", "module": "taskforce_common.task.Task", "name": "task1", "parameters": {}}}
        taskDef2 = {
            "task": {"class": "Task", "module": "taskforce_common.task.Task", "name": "task2", "parameters": {}}}

        engine.taskDeploy(taskDef1)
        engine.taskDeploy(taskDef2)

        engine.taskSetLogLevel("INFO")

        self.assertEqual(logging.INFO, engine.blocks["/task1"].logger.level)
        self.assertEqual(logging.INFO, engine.blocks["/task2"].logger.level)

        engine.taskSetLogLevel("WARNING", "/task2")
        self.assertEqual(logging.INFO, engine.blocks["/task1"].logger.level)
        self.assertEqual(logging.WARNING, engine.blocks["/task2"].logger.level)

        engine.taskSetLogLevel("ERROR")
        self.assertEqual(logging.ERROR, engine.taskGetLogLevel("/task1")["/task1"])
        self.assertEqual(logging.ERROR, engine.taskGetLogLevel("/task2")["/task2"])

    def test_taskSetLogLevel_groupDeploy(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)

        engine.libraryAddModule('task_library.Cowtime', CowTime_source_code)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'two_cows.group'),
                                                          self.fileMap.getMap())
        engine.groupDeploy(definition)

        response = engine.taskSetLogLevel("WARNING")
        self.assertEqual(logging.WARNING, engine.blocks["/two_cows"].getLogLevel())
        self.assertEqual(logging.WARNING, engine.blocks["/two_cows/clara"].getLogLevel())
        self.assertEqual(logging.WARNING, engine.blocks["/two_cows/clementine"].getLogLevel())

        response = engine.taskSetLogLevel("ERROR", "/two_cows/clementine")
        self.assertEqual(logging.WARNING, engine.blocks["/two_cows"].getLogLevel())
        self.assertEqual(logging.WARNING, engine.blocks["/two_cows/clara"].getLogLevel())
        self.assertEqual(logging.ERROR, engine.blocks["/two_cows/clementine"].getLogLevel())

    def test_taskShutdown_withSubTasks(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)

        engine.startup()

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'herd.group'),
                                                          self.fileMap.getMap())

        tasks = []
        for task in definition['group']['tasks']:
            tasks.append(task['task']['name'])

        engine.groupDeploy(definition)
        status = engine.taskStatus()
        self.assertEqual(7, len(status))

        # test shutting down a task and all it's children
        engine.taskShutdown("/herd/TwoCows01")

        start_time = time.time()
        timeout = 10  # seconds

        while (time.time() - start_time < timeout):
            status = engine.taskStatus()
            if len(status) == 4:
                break
            time.sleep(0.1)

        self.assertEqual(4, len(status))
        engine.shutdown()

    def test_taskStartup(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)

        engine.startup()

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'herd.group'),
                                                          self.fileMap.getMap())
        engine.groupDeploy(definition)
        engine.taskStartup("/herd")

        time.sleep(0.5)
        state = engine.blocks["/herd/TwoCows02/clementine"].state
        self.assertEqual(state, TaskState.COMPLETE)
        engine.shutdown()

    def test_taskStatus(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)
        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'herd.group'),
                                                          self.fileMap.getMap())
        engine.groupDeploy(definition)

        status = engine.taskStatus()
        self.assertEqual(7, len(status))

        status = engine.taskStatus("/herd")
        self.assertEqual(1, len(status))

    def test_taskStop(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=ENGINE_LOCAL_LIBRARY_PATH)
        engine.startup()

        engine.libraryAddModule('test_library.TestTaskWithLoop', TestTaskWithLoop_source_code)

        taskDef1 = {
            "task": {
                "class": "TestTaskWithLoop",
                "module": "test_library.TestTaskWithLoop",
                "name": "task1",
                "parameters": {}
            }
        }

        engine.taskDeploy(taskDef1)
        engine.taskStartup("/task1")
        time.sleep(0.1)
        self.assertEqual(TaskState.RUNNING, engine.blocks["/task1"].state)
        engine.taskStop("/task1")
        time.sleep(0.2)
        self.assertEqual(TaskState.STOPPED, engine.blocks["/task1"].state)

        engine.shutdown()

    def test_unregisterBlock(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'herd.group'),
                                                          self.fileMap.getMap())
        engine.groupDeploy(definition)

        self.assertEqual(7, len(engine.blocks))

        subscriptions = engine.subscriptions

        engine._unregisterBlock(engine.blocks['/herd/TwoCows01/clara'])
        self.assertEqual(6, len(engine.blocks))

        engine._unregisterBlock(engine.blocks['/herd/TwoCows01/clementine'])
        self.assertEqual(5, len(engine.blocks))

        engine.groupDeploy(definition)

    def test_unregisterGroup(self):
        engine = Engine(commandPorts=[self.engineServerPort], statusPorts=[self.engineStatusPort],
                        libraryPath=TEST_FOLDER)

        definition = GroupDefinitionLoader.loadDefinition(os.path.join('block_library', 'herd.group'),
                                                          self.fileMap.getMap())
        engine.groupDeploy(definition)

        subscriptions = engine.subscriptions
        self.assertEqual(6, len(engine.getSubscriptions()))

        engine._unregisterGroup(engine.blocks['/herd/TwoCows02'])

        self.assertEqual(4, len(engine.blocks))
        self.assertEqual(3, len(engine.getSubscriptions()))


if __name__ == "__main__":
    from taskforce_common.logcfg import configure_common_logging

    configure_common_logging()
    unittest.main()
